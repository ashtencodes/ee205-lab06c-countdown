///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//
//
// @author Ashten Akemoto <aakemoto@hawaii.edu>
// @date   02-21-2022
///////////////////////////////////////////////////////////////////////////////
#include <time.h>
#include <unistd.h>
#include <stdio.h>

struct tm reftime;

int main() {
   int seconds, diffYear, diffDay, diffHour, diffMin;
   reftime.tm_year = ((2023) - 1900);
   reftime.tm_mon = ((2) - 1);
   reftime.tm_mday = 21;
   reftime.tm_hour = 16;
   reftime.tm_min = 5;
   reftime.tm_sec = 0;

   time_t referenceTime = mktime(&reftime);

   printf("Reference time (HST): %s\n", asctime(&reftime));

   while(1){
      time_t currentTime = time(0);
      seconds = difftime(referenceTime, currentTime);
      if(seconds < 0){
         seconds = -seconds;
      }

      diffYear = seconds/31536000;
      seconds = seconds%31536000;
      diffDay = seconds/86400;
      seconds = seconds%86400;
      diffHour = seconds/3600;
      seconds = seconds%3600;
      diffMin = seconds/60;
      seconds = seconds%60;

      printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n", diffYear, diffDay, diffHour, diffMin, seconds);

      sleep(1);
   }
   return 0;
}
